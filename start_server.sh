#!/bin/sh

./gradlew distZip && \                                                                                                                                                   130 ↵
    unzip -o build/distributions/money-transfer.zip -d build/distOut && \
    ./build/distOut/money-transfer/bin/money-transfer

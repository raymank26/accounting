import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.11")
    application
    distribution
}

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("io.javalin:javalin:2.8.0")

    implementation("com.fasterxml.jackson.core:jackson-databind:2.9.8")

    implementation("org.slf4j:slf4j-simple:1.7.26")

    testImplementation("org.jetbrains.kotlin:kotlin-test")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    testImplementation("com.github.kittinunf.fuel:fuel:2.1.0")

    testImplementation("com.github.kittinunf.fuel:fuel-jackson:2.1.0")
}

application {
    mainClassName = "money.transfer.AppKt"
}

val compileKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.languageVersion = "1.2"

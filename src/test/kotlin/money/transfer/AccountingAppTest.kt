package money.transfer

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.jackson.responseObject
import com.github.kittinunf.result.Result
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.fail

private const val APP_ENDPOINT = "http://localhost:$HTTP_PORT"
private const val UNKNOWN_USER_ID = 482828L
private val OBJECT_MAPPER = ObjectMapper()

/**
 * Date: 2019-05-31.
 */
class AccountingAppTest {

    private lateinit var app: AccountingApp

    @Before
    fun setUp() {
        app = AccountingApp()
        app.startServer()
    }

    @After
    fun tearDown() {
        app.stopServer()
    }

    @Test
    fun testSimpleTransfer() {
        processTransfer("id_key1", USER_ID1, USER_ID2, 10)
        assertEquals(0, getBalance(USER_ID1))
        assertEquals(60, getBalance(USER_ID2))
    }

    @Test
    fun testTransferBack() {
        processTransfer("id_key1", USER_ID1, USER_ID2, 10)
        assertEquals(0, getBalance(USER_ID1))
        assertEquals(60, getBalance(USER_ID2))

        processTransfer("id_key2", USER_ID2, USER_ID1, 10)
        assertEquals(10, getBalance(USER_ID1))
        assertEquals(50, getBalance(USER_ID2))
    }

    @Test
    fun testDuplicateTransfer() {
        processTransfer("key1", USER_ID1, USER_ID2, 10)
        assertEquals(0, getBalance(USER_ID1))
        assertEquals(60, getBalance(USER_ID2))

        processTransfer("key1", USER_ID1, USER_ID2, 10)
        assertEquals(0, getBalance(USER_ID1))
        assertEquals(60, getBalance(USER_ID2))
    }

    @Test
    fun testDuplicateDifferentParams() {
        processTransfer("key1", USER_ID1, USER_ID2, 10)
        assertEquals(0, getBalance(USER_ID1))
        assertEquals(60, getBalance(USER_ID2))

        expectExceptionKey("idempotence.key.mismatch") {
            processTransfer("key1", USER_ID1, USER_ID2, 20)
        }
    }

    @Test
    fun testBalanceOfUnknownUser() {
        expectExceptionKey("account.not.found") {
            getBalance(UNKNOWN_USER_ID)
        }
    }

    @Test
    fun testNotEnoughMoney() {
        expectExceptionKey("not.enough.money") {
            processTransfer("key1", USER_ID1, USER_ID2, 1024)
        }
    }

    @Test
    fun testSameAccounts() {
        expectExceptionKey("same.accounts") {
            processTransfer("same.accounts", USER_ID1, USER_ID1, 10)
        }
    }

    @Test
    fun testAccountDoesNotExist() {
        expectExceptionKey("account.not.found") { processTransfer("key1", UNKNOWN_USER_ID, USER_ID2, 10) }
        expectExceptionKey("account.not.found") { processTransfer("key1", USER_ID1, UNKNOWN_USER_ID, 10) }
    }

    private fun processTransfer(idempotenceKey: String, senderUserId: Long, receiverUserId: Long, amount: Int) {
        val response = Fuel.post("$APP_ENDPOINT/payments/create",
                listOf(
                        Pair("idempotenceKey", idempotenceKey),
                        Pair("senderUserId", senderUserId),
                        Pair("receiverUserId", receiverUserId),
                        Pair("amount", amount)
                )
        ).responseString()
        val result = response.third
        if (result is Result.Failure) {
            throwResponseException(result)
        }
    }

    private fun getBalance(userId: Long): Int {
        return when (val result = Fuel.get("$APP_ENDPOINT/users/$userId/balance").responseObject<BalanceJson>().third) {
            is Result.Failure -> throwResponseException(result)
            is Result.Success -> result.value.balance
        }
    }

    private fun throwResponseException(result: Result.Failure<FuelError>): Nothing =
            throw ResponseException(OBJECT_MAPPER.readValue(result.error.response.data, ExceptionJson::class.java))

    private fun expectExceptionKey(key: String, f: () -> Unit) {
        try {
            f()
            fail("Exception is expected, but call has been completed successfully")
        } catch (e: ResponseException) {
            assertEquals(key, e.data.type)
        }
    }
}

private data class ResponseException constructor(val data: ExceptionJson) : Exception()
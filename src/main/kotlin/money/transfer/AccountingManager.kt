package money.transfer

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * The class is intended to be thread-safe. In the absence of MVCC (Multiversion concurrency control) in JDK we can use
 * atomic idempotence key checking and RW-locks to safely update accounts' states.
 */
class AccountingManager(initAccounts: List<Account>) {

    private val transactions = ConcurrentHashMap<String, Transfer>()
    private val accountStates = ConcurrentHashMap<Long, AccountState>()

    init {
        for (account in initAccounts) {
            this.accountStates[account.userId] = AccountState(account, ReentrantReadWriteLock())
        }
    }

    /**
     * Processes a [transfer] given.
     *
     * @return `true` if the [transfer] processed for the first time or `false` otherwise.
     */
    fun processTransfer(transfer: Transfer): Boolean {
        if (transfer.receiverUserId == transfer.senderUserId) {
            throw SameAccountsException()
        }
        var isFirstTimeProcessed = true
        transactions.compute(transfer.idempotenceKey) { _, prevTransfer ->
            if (prevTransfer != null) {
                if (prevTransfer == transfer) {
                    isFirstTimeProcessed = false
                    return@compute prevTransfer
                } else {
                    throw IdempotenceKeyMismatch()
                }
            }
            val senderState = getAccountState(transfer.senderUserId)
            val receiverState = getAccountState(transfer.receiverUserId)

            val (lockAccount1, lockAccount2) = if (senderState.account.userId < receiverState.account.userId) {
                Pair(senderState.rwLock, receiverState.rwLock)
            } else {
                Pair(receiverState.rwLock, senderState.rwLock)
            }

            lockAccount1.write {
                lockAccount2.write {
                    if (senderState.account.balance < transfer.amount) {
                        throw NotEnoughMoneyException(senderState.account.userId)
                    }
                    senderState.account.decreaseBalance(transfer.amount)
                    receiverState.account.increaseBalance(transfer.amount)
                }
            }
            transfer
        }
        return isFirstTimeProcessed
    }

    /**
     * Returns balance for the account with a [userId] given
     */
    fun getBalance(userId: Long): Int {
        val accountState = getAccountState(userId)
        return accountState.rwLock.read {
            accountState.account.balance
        }
    }

    private fun getAccountState(userId: Long): AccountState {
        return accountStates[userId] ?: throw AccountDoesNotExistException(userId)
    }
}

private data class AccountState(val account: Account, val rwLock: ReentrantReadWriteLock)
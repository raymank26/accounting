package money.transfer

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import io.javalin.Javalin
import org.slf4j.LoggerFactory
import java.lang.Exception

const val USER_ID1 = 57284L
const val USER_ID2 = 84729L
const val HTTP_PORT = 7000

private val LOG = LoggerFactory.getLogger(AccountingApp::class.java)

/**
 * Date: 2019-05-30.
 */
class AccountingApp {

    private lateinit var javalinInstance: Javalin

    fun startServer() {
        val initAccounts = listOf(
                Account(userId = USER_ID1, balance = 10),
                Account(userId = USER_ID2, balance = 50)
        )
        val accountingManager = AccountingManager(initAccounts)
        javalinInstance = Javalin.create().start(HTTP_PORT)

        javalinInstance.exception(Exception::class.java) { e, ctx ->
            ctx.status(400)
            when (e) {
                is AccountDoesNotExistException -> ctx.json(ExceptionJson("account.not.found", e.message))
                is NotEnoughMoneyException -> ctx.json(ExceptionJson("not.enough.money", e.message))
                is SameAccountsException -> ctx.json(ExceptionJson("same.accounts", e.message))
                is IdempotenceKeyMismatch -> ctx.json(ExceptionJson("idempotence.key.mismatch", e.message))
                else -> {
                    LOG.warn("Unable to handle request", e)
                    ctx.status(500)
                    ctx.json(ExceptionJson("internal.error", null))
                }
            }
        }

        javalinInstance.get("/users/:userid/balance") { ctx ->
            val userId = ctx.pathParam<Long>("userid").get()
            val balance = accountingManager.getBalance(userId)
            ctx.json(BalanceJson(balance))
        }

        javalinInstance.post("/payments/create") { ctx ->
            val idempotenceKey = ctx.formParam<String>("idempotenceKey").get()
            val senderUserId = ctx.formParam<Long>("senderUserId").get()
            val receiverUserId = ctx.formParam<Long>("receiverUserId").get()
            val amount = ctx.formParam<Int>("amount").get()
            val firstTime = accountingManager.processTransfer(Transfer(idempotenceKey, amount, senderUserId, receiverUserId))
            if (!firstTime) {
                LOG.debug("Transfer with idempotence key = $idempotenceKey is already processed")
            }
        }
        LOG.info("Server started, accounts in memory = $initAccounts")
    }

    fun stopServer() {
        javalinInstance.stop()
    }
}

data class BalanceJson(val balance: Int)

data class ExceptionJson @JsonCreator constructor(
        @param:JsonProperty("type") val type: String,
        @param:JsonProperty("msg") val msg: String?
)
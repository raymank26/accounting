package money.transfer

class Account(val userId: Long, balance: Int) {
    var balance = balance
        private set

    fun increaseBalance(amount: Int) {
        balance += amount
    }

    fun decreaseBalance(amount: Int) {
        balance -= amount
    }

    override fun toString(): String {
        return "Account(userId=$userId, balance=$balance)"
    }
}

data class Transfer(val idempotenceKey: String, val amount: Int, val senderUserId: Long, val receiverUserId: Long)

data class AccountDoesNotExistException(val userId: Long) : Exception("Account with userId = $userId doesn't exist")

data class NotEnoughMoneyException(val userId: Long) : Exception("Account with userId = $userId doesn't have enough money")

class SameAccountsException : Exception("Transfer accounts are the same")

class IdempotenceKeyMismatch : Exception("Idempotence key provided has been already processed but with different parameters")
